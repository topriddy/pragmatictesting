package com.learn.iloveyouboss;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

import org.junit.Test;

public class ScoreCollectionTest {

    @Test
    public void answersTheMeanOfTwoNumbers() {
        //arrange
        ScoreCollection scores = new ScoreCollection();
        scores.add(() -> 5);
        scores.add(() -> 7);

        //act
        int actualResult = scores.arithmeticMean();

        //assert
        assertThat(actualResult, equalTo(6));
    }
}