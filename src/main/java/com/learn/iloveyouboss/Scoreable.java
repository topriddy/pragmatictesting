package com.learn.iloveyouboss;

@FunctionalInterface
public interface Scoreable {
    int getScore();
}
